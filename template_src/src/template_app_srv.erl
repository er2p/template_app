%%% coding: utf-8
%%% @author {{author_name}} <{{author_email}}>
%%% @date {{date}}
%%% @doc

-module({{name}}_srv).
-author('{{author_name}} <{{author_email}}>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/{{name}}.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    State = #{},
    ?OUT("~p. Inited. Local: ~p (~p).", [?ServiceName, ?MODULE, self()]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------
handle_cast(_Request, State) ->
    ?OUT("~p. RCV unknown CAST:~p",[?ServiceName,_Request]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------
handle_info(_Info, State) ->
    ?OUT("~p. RCV unknown INFO:~p",[?ServiceName,_Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================
